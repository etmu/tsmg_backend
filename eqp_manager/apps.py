from django.apps import AppConfig


class EqpManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eqp_manager'
