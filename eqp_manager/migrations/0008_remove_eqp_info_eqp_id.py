# Generated by Django 5.0.3 on 2024-05-01 09:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eqp_manager', '0007_alter_eqp_usage_usage_info'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eqp_info',
            name='eqp_id',
        ),
    ]
