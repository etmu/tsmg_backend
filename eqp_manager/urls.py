from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('eqp_list/', views.eqpList, name='eqp_list'),
    path('edit_eqp/', views.editEqp, name='edit_eqp'),
    path('delete_eqp/', views.deleteEqp, name='add_new_eqp'),
    path('add_new_eqp/', views.addNewEqp, name='add_new_eqp'),
    path('add_eqp_usage/', views.addEqpUsage, name='add_eqp_usage'),
    path('update_eqp_usage/', views.updateEqpUsage, name='add_eqp_usage'),
    path('get_eqp_usage/', views.getEqpUsage, name='get_eqp_usage'),
    # path('/',                   views.test)
]
