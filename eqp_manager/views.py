from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from django.db.models import Max

from django.forms import model_to_dict


import json

from . import models

# Create your views here.


def eqpList(req: HttpRequest):
    """
        返回仪器列表
    """

    eqp_list = models.Eqp_Info.objects.all().values()

    return HttpResponse(json.dumps(list(eqp_list), ensure_ascii=False))


def editEqp(req: HttpRequest):
    """
        编辑仪器信息
    """
    eqp_info = json.loads(req.body.decode('utf-8'))

    models.Eqp_Info.objects.filter(id=eqp_info['id']).update(**eqp_info)

    return HttpResponse('edit success')


def addNewEqp(req: HttpRequest):
    """
        增加新的仪器
    """

    eqp_info = json.loads(req.body.decode('utf-8'))

    models.Eqp_Info.objects.create(**eqp_info)

    return HttpResponse('add success')


def deleteEqp(req: HttpRequest):
    """
        根据id删除设备记录
    """
    eqp_id = json.loads(req.body.decode("utf-8"))

    models.Eqp_Info.objects.filter(**eqp_id).delete()

    return HttpResponse("delete ok")


def getEqpUsage(req: HttpRequest):
    """
        获取仪器的所有使用信息
    """
    req_body = json.loads(req.body.decode("utf-8"))

    usage_data = models.Eqp_Usage.objects.all().filter(
        eqp_id=req_body['eqp_id']).values()

    print('get eqp usage:', json.dumps(list(usage_data), ensure_ascii=False))

    return HttpResponse(json.dumps(list(usage_data), ensure_ascii=False))


def addEqpUsage(req: HttpRequest):
    """
        添加一条使用记录
    """
    req_body = json.loads(req.body.decode('utf-8'))

    print(req_body)

    models.Eqp_Usage.objects.create(**req_body)

    return HttpResponse('添加使用记录成功')


def updateEqpUsage(req: HttpRequest):
    """
        更新使用信息
    """

    usage = json.loads(req.body.decode('utf-8'))

    print(req.body.decode('utf-8'))

    models.Eqp_Usage.objects.filter(id=usage['id']).update(**usage)

    return HttpResponse('更新使用信息成功!')

