from django.db import models

# Create your models here.


class Project(models.Model):
    project_name = models.CharField(max_length=50, default='')
    deleted = models.BooleanField(default=False)
