from django.urls import path
from . import views

urlpatterns = [
    path("project_list/", view=views.projectList, name="project_list"),
    path("add_project/", view=views.addProject, name="add_project"),
    path("get_project_name/", view=views.getProjectName, name="get_project_name"),
    path("delete_project/", view=views.deleteProject, name="delete_project"),
    path("update_project/", view=views.updateProject, name="update_project"),
]
