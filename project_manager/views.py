from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from django.core import serializers

from .models import Project

import json

# Create your views here.


def projectList(req: HttpRequest):
    """
        返回项目列表
    """
    projects = Project.objects.all()

    return HttpResponse(json.dumps(list(projects.values())))


def addProject(req: HttpRequest):
    """
        添加项目记录
    """
    project = json.loads(req.body.decode("utf-8"))

    p: Project = Project.objects.create(**project)

    return HttpResponse(p.id)


def getProjectName(req: HttpRequest):
    """
        根据id获取project name
    """
    project_id = json.loads(req.body.decode("utf-8"))['id']

    project = Project.objects.get(id=project_id)
    return HttpResponse(project.project_name)


def deleteProject(req: HttpRequest):
    """
        删除项目记录
    """
    project_id = json.loads(req.body.decode("utf-8"))['id']
    Project.objects.filter(id=project_id).update(deleted=True)

    return HttpResponse("delete project ok")


def updateProject(req: HttpRequest):
    """
        更新项目记录
    """
    project_info = json.loads(req.body.decode("utf-8"))
    Project.objects.filter(id=project_info["id"]).update(**project_info)

    return HttpResponse("update project ok")
