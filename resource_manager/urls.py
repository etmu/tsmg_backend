from django.urls import path


from . import views

urlpatterns = [
    # test
    path("test/", views.test, name="users"),

    # 资源相关
    path("resource/<str:rid>/", views.getResource, name="getResource"),
]
