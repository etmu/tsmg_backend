import json
import datetime

from pathlib import Path

# from django.core import serializers
from django.http import HttpRequest, HttpResponse
from django.forms import model_to_dict

from . import models

# Create your views here.


def test(request: HttpRequest):
    """
    测试
    """
    return HttpResponse("OK")


def getResource(request: HttpRequest, rid):
    """
    根据 rid 返回资源信息
    """
    net_env_diagram_data = open(
        Path(__file__).resolve().parent.parent / "resource" / "netenv.png", "rb"
    ).read()
    return HttpResponse(net_env_diagram_data, content_type="image/png")
