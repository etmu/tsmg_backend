from django.apps import AppConfig


class StreamMachineManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'stream_machine_manager'
