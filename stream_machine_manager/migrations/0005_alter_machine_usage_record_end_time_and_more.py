# Generated by Django 5.0 on 2024-01-02 00:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stream_machine_manager', '0004_stream_machine_last_freq_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='machine_usage_record',
            name='end_time',
            field=models.CharField(default='2024/01/02 08:45:41', max_length=20),
        ),
        migrations.AlterField(
            model_name='machine_usage_record',
            name='start_time',
            field=models.CharField(default='2024/01/02 08:45:41', max_length=20),
        ),
    ]
