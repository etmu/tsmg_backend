from django.db import models

from datetime import datetime


# Create your models here.
class User(models.Model):
    user_id = models.IntegerField(default=1)
    ip_addredss = models.CharField(max_length=20, default="192.168.166.2")
    username = models.CharField(max_length=20)
    username_nick = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    comment = models.TextField(max_length=100)
    role = models.IntegerField(default=1)


class Machine_Usage_Record(models.Model):
    id = models.AutoField(primary_key=True)
    machine_id = models.IntegerField(default=1)
    present_format = models.TextField(default="")
    freq = models.IntegerField(default=1000)
    using_user_id = models.IntegerField(default=1)
    start_time = models.CharField(
        default='2024/01/02 08:45:41', max_length=20
    )  # datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    end_time = models.CharField(
        default='2024/01/02 08:45:41', max_length=20
    )
    comment = models.TextField(max_length=500)


class Stream_Machine(models.Model):
    machine_id = models.IntegerField(default=1)
    ip_address = models.CharField(max_length=20, default="/")
    remote_url = models.CharField(max_length=200, default="/")
    machine_summary = models.CharField(max_length=500, default="/")
    weight = models.IntegerField(default=1)
    remote_account = models.CharField(max_length=500, default="test")
    remote_passwd = models.CharField(max_length=500, default="test")
    command = models.TextField(default="/", max_length=5000)
    formats_available = models.TextField(max_length=300, default="[]")
    edit_lock = models.TextField(default="/")
    last_freq = models.IntegerField(default=0)
