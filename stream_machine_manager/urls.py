from django.urls import path


from . import views

urlpatterns = [
    # user
    path("users/", views.users, name="users"),
    path("login/", views.login, name="login"),

    # stream machine
    path("machines/", views.machines, name="machines"),
    path("change_machine/", views.changeMachine, name="changeMachine"),
    path("machine/<int:id>", views.getMachine, name="getMachine"),
    path("machine_update/", views.updateMachine, name="machine_update"),
    path("usage_records/", views.usageRecords, name="usage_records"),
    path("add_new_usage/", views.addNewUsage, name="add_new_usage"),
    path("cancel_usage/<int:id>", views.cancelUsage, name="cancel_usage"),
    path("change_usage/", views.changeUsage, name="add_new_usage"),
    path("add_new_machine/", views.addNewMachine, name="add_new_machine"),
    path("update_machine/<int:id>/", views.updateMachine, name="updateMachine"),
    path("machine_edit_lock/<int:machine_id>/",
         views.machineEditLock, name="machineEditLock"),
    path("machine_edit_unlock/<int:machine_id>/",
         views.machineEditUnlock, name="machineEditUnlock"),
    path("platform/dev/a/ping", views.ping, name="machineEditUnlock"),

]
