import json
import datetime

from pathlib import Path

# from django.core import serializers
from django.http import HttpRequest, HttpResponse
from django.forms import model_to_dict

from . import models

time_format = "%Y/%m/%d %H:%M:%S"


def test(request: HttpRequest):
    # time_str = datetime()
    time_format = "%Y/%m/%d %H:%M:%S"
    print(datetime.datetime.now())
    time_str = datetime.datetime.now().strftime(time_format)

    print(time_str)
    print(datetime.datetime.strptime(time_str, time_format))
    return HttpResponse(time_str)


def users(request: HttpRequest):
    """
    返回所有的用户
    """
    users = models.User.objects.all().values()
    return HttpResponse(json.dumps(list(users), ensure_ascii=False))


def login(request: HttpRequest):
    """
    登录
    """
    req_body = json.loads(request.body.decode("utf-8"))
    user_id = req_body["user_id"]

    user: models.User = models.User.objects.get(user_id=user_id)

    return HttpResponse(1 if req_body["passwd"] == user.password else 0)


def addNewUser(request: HttpRequest):
    """
    添加新用户
    """

    models.User.objects.create(**json.loads(request.body.decode("utf-8")))
    return HttpResponse("add new user")


def getUser(request: HttpRequest, id):
    """
    返回一个用户
    """
    user = models.User.objects.filter(machine_id=id).values()
    return HttpResponse(json.dumps(list(user), ensure_ascii=False))


def getMachine(request: HttpRequest, id):
    """
    返回某一个码流机
    """
    machine = models.Stream_Machine.objects.filter(machine_id=id).values()
    return HttpResponse(json.dumps(list(machine), ensure_ascii=False))


def machines(request: HttpRequest):
    """
    返回所有的码流机
    """
    machines = models.Stream_Machine.objects.all().values()
    return HttpResponse(json.dumps(list(machines), ensure_ascii=False))


def changeMachine(request: HttpRequest):
    """
    更改码流机信息
    """
    post_body = json.loads(request.body.decode("utf-8"))
    models.Stream_Machine.objects.filter(machine_id=post_body["machine_id"]).update(
        **post_body
    )

    print(post_body)

    return HttpResponse("change success")


def changeLastFreq(machine_id, freq):
    """
        更改码流机的上次使用频点
    """
    models.Stream_Machine.objects.filter(
        machine_id=machine_id).update(last_freq=freq)


def addNewUsage(request: HttpRequest):
    """
    添加新的使用记录
    """
    usage_info = json.loads(request.body.decode("utf-8"))

    machine_id = usage_info['machine_id']
    freq = usage_info['freq']
    changeLastFreq(machine_id, freq)

    print(usage_info)

    models.Machine_Usage_Record.objects.create(**usage_info)

    return HttpResponse("add new usage")


def cancelUsage(request: HttpRequest, id):
    """
    根据前端传来的 usage id ,取消使用记录
    """
    print(id)
    present_time = datetime.datetime.now().strftime(time_format)
    res = models.Machine_Usage_Record.objects.filter(id=id).update(
        end_time=present_time
    )

    return HttpResponse("成功取消使用")


def changeUsage(request: HttpRequest):
    """
    修改使用记录
    """
    usage_info = json.loads(request.body.decode("utf-8"))

    machine_id = usage_info['machine_id']
    freq = usage_info['freq']
    changeLastFreq(machine_id, freq)

    print(usage_info)

    models.Machine_Usage_Record.objects.filter(
        id=usage_info["id"]).update(**usage_info)

    return HttpResponse("change usage")


def usageRecords(request: HttpRequest):
    """
    获取所有的使用记录,包含码流机的基本信息
    TODO 使用sql语句优化
    """
    # 记录当前时间

    present_time = datetime.datetime.now()

    # 先查询出所有的记录
    usages = list(models.Machine_Usage_Record.objects.all().values())
    machines = list(models.Stream_Machine.objects.all().values())

    records = []
    default_record = {
        "present_format": "/",
        "using_user_id": -1,
        "username": "/",
        "freq": 0,
        "start_time": "/",
        "end_time": "/",
        "comment": "/",
        "busy": 0,
        "remain_time": 0,
        "usage_id": -1,
    }

    # 查询最近的使用记录
    for machine in machines:
        # 过滤出当前使用的
        present_usage = list(
            filter(
                lambda usage: usage["machine_id"] == machine["machine_id"]
                and datetime.datetime.strptime(usage["start_time"], time_format)
                < present_time
                and datetime.datetime.strptime(usage["end_time"], time_format)
                > present_time,
                usages,
            )
        )

        if len(present_usage) == 0:
            # 没有当前记录的，记录为空闲
            present_usage.append(default_record)
        else:
            # 置为使用中
            present_usage[0]["busy"] = 1
            present_usage[0]["usage_id"] = present_usage[0]["id"]
            remain_time = (
                datetime.datetime.strptime(
                    present_usage[0]["end_time"], time_format)
                - present_time
            )
            present_usage[0]["remain_time"] = int(remain_time.total_seconds())

        # 计算出当前所有码流机的使用情况
        records.append({**machine, **present_usage[0]})

    return HttpResponse(json.dumps(records, ensure_ascii=False))


def updateMachine(request: HttpRequest):
    """
    更新码流机
    """
    return HttpResponse("OK")


def findMachine(request: HttpRequest):
    """
    查找码流机
    """
    return HttpResponse(1)


def machineEditLock(request: HttpRequest, machine_id):
    """
    码流机编辑上锁
    ans
        -1 正在使用
        0  没有上锁
        1  上锁成功
    """
    ans = -1
    machine = models.Stream_Machine.objects.get(machine_id=machine_id)
    present_time = datetime.datetime.now()

    if machine.edit_lock != "/":
        # 编辑锁 不为空
        lock_time = datetime.datetime.strptime(machine.edit_lock, time_format)

        if present_time > lock_time:
            # 锁超时
            print(f"machine {machine_id} 编辑锁超时")
            # 更新锁并锁定 30s
            lock = present_time + datetime.timedelta(seconds=30)
            models.Stream_Machine.objects.filter(machine_id=machine_id).update(
                edit_lock=lock.strftime(time_format)
            )
            # 上锁成功
            ans = 1
    else:
        # 没有上锁,锁定30s
        lock = present_time + datetime.timedelta(seconds=30)
        models.Stream_Machine.objects.filter(machine_id=machine_id).update(
            edit_lock=lock.strftime(time_format)
        )
        # 上锁成功
        ans = 1

    return HttpResponse(ans)


def machineEditUnlock(request: HttpRequest, machine_id):
    """
    码流机编辑解锁
    """
    machine = models.Stream_Machine.objects.get(machine_id=machine_id)
    return HttpResponse(json.dumps(model_to_dict(machine), ensure_ascii=False))


def addNewMachine(request: HttpRequest):
    """
    添加新的码流机
    """
    new_machine = json.loads(request.body.decode("utf-8"))
    models.Stream_Machine.objects.create(**new_machine)
    return HttpResponse("add new machine")


def updateMachine(request: HttpRequest, id: int):
    """
    更新码流机信息
    """
    machine = json.loads(request.body.decode("utf-8"))

    models.Stream_Machine.objects.filter(id=id).update(**machine)
    return HttpResponse("update machines")


def ping(req):
    result = {
        'status': 200,
        'result': 'ok'
    }
    return HttpResponse(json.dumps(result))
