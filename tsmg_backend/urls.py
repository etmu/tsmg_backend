from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path("resource_manager/", include("resource_manager.urls")),
    path("eqp_manager/", include("eqp_manager.urls")),
    path("stream_machine_manager/", include("stream_machine_manager.urls")),
    path("project_manager/", include("project_manager.urls")),
]
